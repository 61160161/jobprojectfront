const userService = {
  userList: [
    {
      isActive: true,
      id: 1,
      username: 'John Danty',
      password: '@Xa1234',
      email: 'John1235@gmail.com',
      work: '2 year',
      telphone: '098-458-5713',
      education: 'University of Cambridge',
      salary: '35,000',
      livein: 'England'
    },
    {
      isActive: false,
      id: 2,
      username: '61160149',
      password: '@Xa1234',
      email: '61160149@go.buu.ac.th',
      work: '0 year',
      telphone: '098-458-5713',
      education: 'Burapha University',
      salary: '-',
      livein: 'Bangkok'
    }
  ],
  lastId: 3,
  addUser (user) {
    user.id = this.lastId++
    this.userList.push(user)
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  deleteUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getUsers () {
    return [...this.userList]
  }
}

export default userService
