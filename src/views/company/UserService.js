const userService = {
  userList: [
    {
      isActive: true,
      id: 1,
      namecompany: 'MarsX Co., Ltd.',
      usernames: 'นาย วิทวัฒน์ มีนวน',
      email: 'marsx@gmail.com',
      website: 'https://google.co.th/',
      telphone: '094-856-7629',
      address: {
        housenum: '4/23',
        road: '-',
        subdistrict: 'ท่าช้าง',
        district: 'เมืองนครนายก',
        province: 'นครนายก',
        postalNumber: '21000'
      }
    },
    {
      isActive: false,
      id: 2,
      namecompany: 'E Commerce Solution Co., Ltd.',
      usernames: 'นาย สุนิชาติ พาสวดี',
      email: 'info@ecommerce-solution.co.th',
      website: 'https://ecommerce-solution.co.th/',
      telphone: '094-954-7891',
      address: {
        housenum: '6/24',
        road: 'กำแพงเพชร6',
        subdistrict: 'แขวงลาดยาว',
        district: 'เขตจตุจักร',
        province: 'กรุงเทพ',
        postalNumber: '10900'
      }
    }
  ],
  lastId: 3,
  addUser (user) {
    user.id = this.lastId++
    this.userList.push(user)
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  deleteUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getUsers () {
    return [...this.userList]
  }
}

export default userService
