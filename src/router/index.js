import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home.vue'
import company from '../views/company/Index.vue'
import post from '../views/company/Jobpost.vue'
import details from '../views/Workdetail.vue'
import register from '../views/Register.vue'
import login from '../views/Login.vue'
import result from '../views/resultSearch.vue'
import consider from '../views/Consider.vue'
import userTest from '../views/userTest.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/company',
      name: 'Company',
      component: company
    },
    {
      path: '/Workdetail/:jid',
      name: 'Workdetail',
      component: details
    },
    {
      path: '/register',
      name: 'Register',
      component: register
    },
    {
      path: '/login',
      name: 'Login',
      component: login
    },
    {
      path: '/result',
      name: 'Result',
      component: result
    },
    {
      path: '/user',
      name: 'userTest',
      component: userTest
    },
    {
      path: '/consider',
      name: 'consider',
      component: consider
    },
    {
      path: '/post',
      name: 'jobpost',
      component: post
    }
  ]
})
